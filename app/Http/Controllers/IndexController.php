<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index() {
        $data = DB::table('web_scrape_kapel')
            ->select( DB::raw('bintang as Rating, count(judul) as Jumlah')  )
            ->groupBy('bintang')
            ->get();
        return view('me', [
            'data' => json_encode($data)
        ]);
    }

    public function testing() {
        $data = DB::table('bank')
            ->select( DB::raw('bintang as Rating, count(judul) as Jumlah')  )
            ->groupBy('bintang')
            ->get();
        return json_encode($data);
    }

    public function covid() {
        //"SELECT  FROM `covids19` WHERE `Country/Region` = 'Indonesia' or `Country/Region` = '' or `Country/Region` = '' GROUP  BY `Country/Region`"
        $data = DB::table('covids19')
            ->select( DB::raw('`Country/Region` as `Country`, SUM(Confirmed) as `Confirmed`')  )
            ->orWhere('Country/Region', 'Indonesia')
            ->orWhere('Country/Region', 'Japan')
            ->orWhere('Country/Region', 'South Korea')
            ->groupBy('Country/Region')
            ->get();
        return view('covid', [
            'dataset' => json_encode($data)
        ]);
    }
}
