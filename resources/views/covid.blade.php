<html>
<head>
    <title>Hello</title>
    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(function () {
                drawChart({!! $dataset !!});
            });

            function drawChart(covid) {

                let data = new google.visualization.DataTable();

                data.addColumn('string', 'Country');
                data.addColumn('number', 'Confirmed');

                let data_arr = [];

                $.each(covid, function (i, obj) {
                    data_arr.push([obj.Country, parseInt(obj.Confirmed)]);
                });

                data.addRows(data_arr);

                const pie_options = {
                    title: 'Covid 19',
                    is3D: true,
                };

                const chart = new google.visualization.PieChart(document.getElementById('piechart'));

                chart.draw(data, pie_options);
            }
        });


    </script>
</head>
<body>
<div id="piechart" style="width: 900px; height: 500px;"></div>
</body>
</html>
