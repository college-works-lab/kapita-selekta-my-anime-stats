<html>
<head>
    <title> Google Chart</title>

    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {


            google.charts.load('current', {
                'packages' : ['corechart']
            });

            google.charts.setOnLoadCallback(function() {
                drawChart({!! $data !!});
            });

            function drawChart(result) {
                let data = new google.visualization.DataTable();

                data.addColumn('string' , 'Rating');
                data.addColumn('number' , 'Jumlah');

                var dataArray = [];

                $.each(result, function ( i, obj) {
                    dataArray.push([ obj.Rating, parseInt(obj.Jumlah)]);
                });

                data.addRows(dataArray);


                // Pie Char t
                var pie_options = {
                    title: 'My Rating',
                    legend : {position: 'bottom'}
                };

                var piechart = new google.visualization.PieChart(document.getElementById('piechart_div'));
                piechart.draw(data, pie_options);


                // Bar chart
                var bar_options = {
                    title: "My Rating",
                    width: 600,
                    height: 400,
                    bar: {groupWidth: "95%"},
                    legend: { position: "none" },
                };
                var barchart = new google.visualization.BarChart(document.getElementById("barchart_values"));
                barchart.draw(data, bar_options);


                // Column chart
                var column_options = {
                    title: "My Rating",
                    width: 600,
                    height: 400,
                    bar: {groupWidth: "95%"},
                    legend: { position: "none" },
                };
                var column_chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
                column_chart.draw(data, column_options);

// Area chart
                var area_options = {
                    title: "My Rating",
                    hAxis: {title: 'Rating',  titleTextStyle: {color: '#333'}},
                    vAxis: {minValue: 0}
                };

                var area_chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                area_chart.draw(data, area_options);

            }

        });

    </script>
</head>

<body>

<table>
    <tr>
        <td>
            <div id="piechart_div" style="border: 1px solid #ccc"></div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="barchart_values" style="border: 1px solid #ccc"></div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="columnchart_values" style="border: 1px solid #ccc"></div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="chart_div" style="border: 1px solid #ccc"></div>
        </td>
    </tr>

</table>
</body>
</html>
